#pragma once
#include "stdafx.h"
#include <vector>
#include <fstream>
#include <string>
#include <iostream>

struct pixel {
	float Red, Green, Blue;
};

class HTMLTableReader
{
public:
	HTMLTableReader();
	~HTMLTableReader();

	bool loadTable(const char* filename);
	unsigned int getSize() const;
	std::vector<pixel*> getImg() const;
	int getWidth() const;
	int getHeight() const;

private:
	std::vector<pixel*>img;
	unsigned int width, height;

	int charToIntHex(char a);
};

