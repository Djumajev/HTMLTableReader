#include "stdafx.h"
#include <vector>
#include <fstream>
#include <string>
#include <iostream>
#include "HTMLTableReader.h"

HTMLTableReader::HTMLTableReader()
{
}


HTMLTableReader::~HTMLTableReader()
{
}


bool HTMLTableReader::loadTable(const char* filename) {
	std::ifstream file(filename);

	if (!file) return false;

	while (file.good()) {
		std::string str;
		std::getline(file, str);

		std::size_t position = str.find("<TR>");
		if (position != std::string::npos) {
			this->height++;
			this->width = 0;
		} else {
			position = str.find("<TD ");
			if (position != std::string::npos) {
				this->width++;

				std::size_t color_start = str.find("BGCOLOR=#");
				if (color_start != std::string::npos) {
					std::string color = str.substr(color_start+9, 6);
					float red = (this->charToIntHex(color[0]) * 16) + this->charToIntHex(color[1]),
						green = (this->charToIntHex(color[2]) * 16) + this->charToIntHex(color[3]),
						blue = (this->charToIntHex(color[4]) * 16) + this->charToIntHex(color[5]);
			
					this->img.push_back(new pixel{red / 256, green / 256, blue / 256});
				}
			}
		}
	}
	return true;
}

std::vector<pixel*> HTMLTableReader::getImg() const {
	return this->img;
}

unsigned int HTMLTableReader::getSize() const{
	return this->width*this->height;
}

int HTMLTableReader::charToIntHex(char a) {
	if (int(a) > 47 && int(a) < 58)
		return a - '0';
	else switch (a) {
	case 'a':
		return 10;
		break;
	case 'b':
		return 11;
		break;
	case 'c':
		return 12;
		break;
	case 'd':
		return 13;
		break;
	case 'e':
		return 14;
		break;
	case 'f':
		return 15;
		break;
	default:
		return NULL;
		break;
	}
}

int HTMLTableReader::getWidth() const {
	return this->width;
}

int HTMLTableReader::getHeight() const {
	return this->height;
}